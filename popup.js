
let actualTroopsPopup = {
	spear: {qty: 0, isActif: true},
	sword: {qty: 0, isActif: true},
	axe: {qty: 0, isActif: true},
	archer: {qty: 0, isActif: false},
	light: {qty: 0, isActif: false},
	mArcher: {qty: 0, isActif: false},
	heavy: {qty: 0, isActif: false},
	knight: {qty: 0, isActif: false},
};

// chrome.runtime.onMessage.addListener(messageReceived);

// function messageReceived(msg) {
// 	if (msg.task === "browserOpen") {
//    // Do your work here
// 		let test = document.getElementById("test");
// 		test.innerHTML = JSON.stringify(msg);
// 	}
// }

let slider;


document.addEventListener('DOMContentLoaded', function() {
	console.log('document.addEventListener');
    let initScavenging = document.getElementById("initScavenging");
    initScavenging.addEventListener("click", saveTroops);
    slider = document.getElementById("myRange");
	let output = document.getElementById("sliderOutput");
	output.innerHTML = 'With ' + slider.value + '% of your troops'; // Display the default slider value

	// Update the current slider value (each time you drag the slider handle)
	slider.oninput = function() {
	  output.innerHTML = 'With ' + this.value + '% of your troops';
	}
});

function saveTroops() {
    // let slider = document.getElementById("myRange");
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
	  	chrome.tabs.sendMessage(tabs[0].id, {task: "saveTroops", percent: (slider.value / 100) + ''}, function(response) {
	  	});
	});
}



	  		// actualTroopsPopup = response;
	  		// let troops = response;
		// let test = document.getElementById("test");
		// test.innerHTML = JSON.stringify(response);
		// chrome.storage.sync.set({'actualTroops': JSON.stringify(response)}, function() {
	 //      console.log('actualTroops saved');
	 //    });
	/* Small */
	 //    let spearSmall = document.getElementById("spearSmall");
	 //    // spearSmall.innerHTML = JSON.stringify(response);
		// if (response.spear.qty > 0) {
	 //    	spearSmall.innerHTML = Math.round(response.spear.qty * smallScavengingRatio);
		// } else {
	 //    	spearSmall.innerHTML = '0';
		// }

	 //    let swordSmall = document.getElementById("swordSmall");
		// if (response.sword.qty > 0) {
	 //    	swordSmall.innerHTML = Math.round(response.sword.qty * smallScavengingRatio);
		// } else {
	 //    	swordSmall.innerHTML = '0';
		// }

	 //    let axeSmall = document.getElementById("axeSmall");
		// if (response.axe.qty > 0) {
	 //    	axeSmall.innerHTML = Math.round(response.axe.qty * smallScavengingRatio);
		// } else {
	 //    	axeSmall.innerHTML = '0';
		// }

	 //    let archerSmall = document.getElementById("archerSmall");
		// if (response.archer.qty > 0) {
	 //    	archerSmall.innerHTML = Math.round(response.archer.qty * smallScavengingRatio);
		// } else {
	 //    	archerSmall.innerHTML = '0';
		// }

	 //    let lightSmall = document.getElementById("lightSmall");
		// if (response.light.qty > 0) {
	 //    	lightSmall.innerHTML = Math.round(response.light.qty * smallScavengingRatio);
		// } else {
	 //    	lightSmall.innerHTML = '0';
		// }
		
	 //    let mArcherSmall = document.getElementById("mArcherSmall");
		// if (response.mArcher.qty > 0) {
	 //    	mArcherSmall.innerHTML = Math.round(response.mArcher.qty * smallScavengingRatio);
		// } else {
	 //    	mArcherSmall.innerHTML = '0';
		// }
		
	 //    let heavySmall = document.getElementById("heavySmall");
		// if (response.heavy.qty > 0) {
	 //    	heavySmall.innerHTML = Math.round(response.heavy.qty * smallScavengingRatio);
		// } else {
	 //    	heavySmall.innerHTML = '0';
		// }
	/* Small End */

	/* Medium */
	 //    let spearMedium = document.getElementById("spearMedium");
		// if (response.spear.qty > 0) {
	 //    	spearMedium.innerHTML = Math.round(response.spear.qty * mediumScavengingRatio);
		// } else {
	 //    	spearMedium.innerHTML = '0';
		// }

	 //    let swordMedium = document.getElementById("swordMedium");
		// if (response.sword.qty > 0) {
	 //    	swordMedium.innerHTML = Math.round(response.sword.qty * mediumScavengingRatio);
		// } else {
	 //    	swordMedium.innerHTML = '0';
		// }

	 //    let axeMedium = document.getElementById("axeMedium");
		// if (response.axe.qty > 0) {
	 //    	axeMedium.innerHTML = Math.round(response.axe.qty * mediumScavengingRatio);
		// } else {
	 //    	axeMedium.innerHTML = '0';
		// }

	 //    let archerMedium = document.getElementById("archerMedium");
		// if (response.archer.qty > 0) {
	 //    	archerMedium.innerHTML = Math.round(response.archer.qty * mediumScavengingRatio);
		// } else {
	 //    	archerMedium.innerHTML = '0';
		// }

	 //    let lightMedium = document.getElementById("lightMedium");
		// if (response.light.qty > 0) {
	 //    	lightMedium.innerHTML = Math.round(response.light.qty * mediumScavengingRatio);
		// } else {
	 //    	lightMedium.innerHTML = '0';
		// }
		
	 //    let mArcherMedium = document.getElementById("mArcherMedium");
		// if (response.mArcher.qty > 0) {
	 //    	mArcherMedium.innerHTML = Math.round(response.mArcher.qty * mediumScavengingRatio);
		// } else {
	 //    	mArcherMedium.innerHTML = '0';
		// }
		
	 //    let heavyMedium = document.getElementById("heavyMedium");
		// if (response.heavy.qty > 0) {
	 //    	heavyMedium.innerHTML = Math.round(response.heavy.qty * mediumScavengingRatio);
		// } else {
	 //    	heavyMedium.innerHTML = '0';
		// }
	/* Medium End */

	/* Big */
	 //    let spearBig = document.getElementById("spearBig");
		// if (response.spear.qty > 0) {
	 //    	spearBig.innerHTML = Math.round(response.spear.qty * bigScavengingRatio);
		// } else {
	 //    	spearBig.innerHTML = '0';
		// }

	 //    let swordBig = document.getElementById("swordBig");
		// if (response.sword.qty > 0) {
	 //    	swordBig.innerHTML = Math.round(response.sword.qty * bigScavengingRatio);
		// } else {
	 //    	swordBig.innerHTML = '0';
		// }

	 //    let axeBig = document.getElementById("axeBig");
		// if (response.axe.qty > 0) {
	 //    	axeBig.innerHTML = Math.round(response.axe.qty * bigScavengingRatio);
		// } else {
	 //    	axeBig.innerHTML = '0';
		// }

	 //    let archerBig = document.getElementById("archerBig");
		// if (response.archer.qty > 0) {
	 //    	archerBig.innerHTML = Math.round(response.archer.qty * bigScavengingRatio);
		// } else {
	 //    	archerBig.innerHTML = '0';
		// }

	 //    let lightBig = document.getElementById("lightBig");
		// if (response.light.qty > 0) {
	 //    	lightBig.innerHTML = Math.round(response.light.qty * bigScavengingRatio);
		// } else {
	 //    	lightBig.innerHTML = '0';
		// }
		
	 //    let mArcherBig = document.getElementById("mArcherBig");
		// if (response.mArcher.qty > 0) {
	 //    	mArcherBig.innerHTML = Math.round(response.mArcher.qty * bigScavengingRatio);
		// } else {
	 //    	mArcherBig.innerHTML = '0';
		// }
		
	 //    let heavyBig = document.getElementById("heavyBig");
		// if (response.heavy.qty > 0) {
	 //    	heavyBig.innerHTML = Math.round(response.heavy.qty * bigScavengingRatio);
		// } else {
	 //    	heavyBig.innerHTML = '0';
		// }
	/* Big End */

	/* Huge */
	 //    let spearHuge = document.getElementById("spearHuge");
		// if (response.spear.qty > 0) {
	 //    	spearHuge.innerHTML = Math.round(response.spear.qty * hugeScavengingRatio);
		// } else {
	 //    	spearHuge.innerHTML = '0';
		// }

	 //    let swordHuge = document.getElementById("swordHuge");
		// if (response.sword.qty > 0) {
	 //    	swordHuge.innerHTML = Math.round(response.sword.qty * hugeScavengingRatio);
		// } else {
	 //    	swordHuge.innerHTML = '0';
		// }

	 //    let axeHuge = document.getElementById("axeHuge");
		// if (response.axe.qty > 0) {
	 //    	axeHuge.innerHTML = Math.round(response.axe.qty * hugeScavengingRatio);
		// } else {
	 //    	axeHuge.innerHTML = '0';
		// }

	 //    let archerHuge = document.getElementById("archerHuge");
		// if (response.archer.qty > 0) {
	 //    	archerHuge.innerHTML = Math.round(response.archer.qty * hugeScavengingRatio);
		// } else {
	 //    	archerHuge.innerHTML = '0';
		// }

	 //    let lightHuge = document.getElementById("lightHuge");
		// if (response.light.qty > 0) {
	 //    	lightHuge.innerHTML = Math.round(response.light.qty * hugeScavengingRatio);
		// } else {
	 //    	lightHuge.innerHTML = '0';
		// }
		
	 //    let mArcherHuge = document.getElementById("mArcherHuge");
		// if (response.mArcher.qty > 0) {
	 //    	mArcherHuge.innerHTML = Math.round(response.mArcher.qty * hugeScavengingRatio);
		// } else {
	 //    	mArcherHuge.innerHTML = '0';
		// }
		
	 //    let heavyHuge = document.getElementById("heavyHuge");
		// if (response.heavy.qty > 0) {
	 //    	heavyHuge.innerHTML = Math.round(response.heavy.qty * hugeScavengingRatio);
		// } else {
	 //    	heavyHuge.innerHTML = '0';
		// }
	/* Huge End */
	  		// initTroops(actualTroopsPopup);