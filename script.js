const smallScavengingRatio = 0.57692307692307692307692307692308;
const mediumScavengingRatio = 0.23076923076923076923076923076923;
const bigScavengingRatio = 0.11538461538461538461538461538462;
const hugeScavengingRatio = 0.07692307692307692307692307692308;

const copyTheDamnThingStr = '<script type="text/javascript"> function copyTheDamnThing(idButton) { var $temp = $("<input>"); $("body").append($temp); $temp.val($("#" + idButton).text()).select(); document.execCommand("copy"); $temp.remove(); }</script>';


let actualTroops = {
	spear: {qty: 0, isActif: true},
	sword: {qty: 0, isActif: true},
	axe: {qty: 0, isActif: true},
	archer: {qty: 0, isActif: false},
	light: {qty: 0, isActif: false},
	mArcher: {qty: 0, isActif: false},
	heavy: {qty: 0, isActif: false},
	knight: {qty: 0, isActif: false},
};

//let SCAVENGING_STEP = TEST_SCAVENGING;

/* Controller */

// $(document).ready(function() {
// 	$('body').append(copyTheDamnThingStr);
// 	console.log("Script listener done");
// });




	chrome.runtime.onMessage.addListener(
	  function(request, sender, sendResponse) {
			console.log('onMessage', request);
			console.log('onMessage', sender);
	    if (request.task === "saveTroops") {
			actualTroops.spear.qty = Number(getNbTroops($("a[class='units-entry-all'][data-unit='spear']")[0].text));
			actualTroops.spear.qty *= request.percent;
			actualTroops.sword.qty = Number(getNbTroops($("a[class='units-entry-all'][data-unit='sword']")[0].text));
			actualTroops.sword.qty *= request.percent;
			actualTroops.axe.qty = Number(getNbTroops($("a[class='units-entry-all'][data-unit='axe']")[0].text));
			actualTroops.axe.qty *= request.percent;
			actualTroops.archer.qty = Number(getNbTroops($("a[class='units-entry-all'][data-unit='archer']")[0].text));
			actualTroops.archer.qty *= request.percent;
			actualTroops.light.qty = Number(getNbTroops($("a[class='units-entry-all'][data-unit='light']")[0].text));
			actualTroops.light.qty *= request.percent;
			actualTroops.mArcher.qty = Number(getNbTroops($("a[class='units-entry-all'][data-unit='marcher']")[0].text));
			actualTroops.mArcher.qty *= request.percent;
			actualTroops.heavy.qty = Number(getNbTroops($("a[class='units-entry-all'][data-unit='heavy']")[0].text));
			actualTroops.heavy.qty *= request.percent;
			// actualTroops.knight.qty = Number(getNbTroops($("a[class='units-entry-all'][data-unit='knight']")[0].text));

			$('#scavenge_screen').css('display', 'flex');
			const htmlToInject = "<table id='scanvengingTable'><div id='scriptLcnScavenging'></div><thead><tr><th>Collect Type</th>"
				+ (actualTroops.spear.qty > 0 ? "<th>spear</th>" : '') + (actualTroops.sword.qty > 0 ? "<th>sword</th>" : '')
				+ (actualTroops.axe.qty > 0 ? "<th>axe</th>" : '') + (actualTroops.archer.qty > 0 ? "<th>archer</th>" : '')
				+ (actualTroops.light.qty > 0 ? "<th>light</th>" : '') + (actualTroops.mArcher.qty > 0 ? "<th>mArcher</th>" : '')
				+ (actualTroops.heavy.qty > 0 ? "<th>heavy</th>" : '') + "</tr></thead><tbody><tr><td>Small</td>"
				+ (actualTroops.spear.qty > 0 ? "<td><button id='spearSmallButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.spear.qty * smallScavengingRatio) + "</button></td>" : '')
				+ (actualTroops.sword.qty > 0 ? "<td><button id='swordSmallButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.sword.qty * smallScavengingRatio) + "</button></td>" : '')
				+ (actualTroops.axe.qty > 0 ? "<td><button id='axeSmallButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.axe.qty * smallScavengingRatio) + "</button></td>" : '')
				+ (actualTroops.archer.qty > 0 ? "<td><button id='archerSmallButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.archer.qty * smallScavengingRatio) + "</button></td>" : '')
		    	+ (actualTroops.light.qty > 0 ? "<td><button id='lightSmallButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.light.qty * smallScavengingRatio) + "</button></td>" : '')
		    	+ (actualTroops.mArcher.qty > 0 ? "<td><button id='mArcherSmallButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.mArcher.qty * smallScavengingRatio) + "</button></td>" : '')
		    	+ (actualTroops.heavy.qty > 0 ? "<td><button id='heavySmallButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.heavy.qty * smallScavengingRatio) + "</button></td>" : '')
		    	+ "</tr><tr><td>Medium</td>"
		    	+ (actualTroops.spear.qty > 0 ? "<td><button id='spearMediumButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.spear.qty * mediumScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.sword.qty > 0 ? "<td><button id='swordMediumButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.sword.qty * mediumScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.axe.qty > 0 ? "<td><button id='axeMediumButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.axe.qty * mediumScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.archer.qty > 0 ? "<td><button id='archerMediumButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.archer.qty * mediumScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.light.qty > 0 ? "<td><button id='lightMediumButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.light.qty * mediumScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.mArcher.qty > 0 ? "<td><button id='mArcherMediumButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.mArcher.qty * mediumScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.heavy.qty > 0 ? "<td><button id='heavyMediumButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.heavy.qty * mediumScavengingRatio) + "</button></td>" : '')
		       	+ "</tr><tr><td> Big</td>"
		       	+ (actualTroops.spear.qty > 0 ? "<td><button id='spearBigButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.spear.qty * bigScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.sword.qty > 0 ? "<td><button id='swordBigButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.sword.qty * bigScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.axe.qty > 0 ? "<td><button id='axeBigButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.axe.qty * bigScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.archer.qty > 0 ? "<td><button id='archerBigButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.archer.qty * bigScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.light.qty > 0 ? "<td><button id='lightBigButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.light.qty * bigScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.mArcher.qty > 0 ? "<td><button id='mArcherBigButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.mArcher.qty * bigScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.heavy.qty > 0 ? "<td><button id='heavyBigButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.heavy.qty * bigScavengingRatio) + "</button></td>" : '')
		       	+ "</tr><tr><td> Huge</td>"
		       	+ (actualTroops.spear.qty > 0 ? "<td><button id='spearHugeButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.spear.qty * hugeScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.sword.qty > 0 ? "<td><button id='swordHugeButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.sword.qty * hugeScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.axe.qty > 0 ? "<td><button id='axeHugeButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.axe.qty * hugeScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.archer.qty > 0 ? "<td><button id='archerHugeButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.archer.qty * hugeScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.light.qty > 0 ? "<td><button id='lightHugeButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.light.qty * hugeScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.mArcher.qty > 0 ? "<td><button id='mArcherHugeButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.mArcher.qty * hugeScavengingRatio) + "</button></td>" : '')
		       	+ (actualTroops.heavy.qty > 0 ? "<td><button id='heavyHugeButton' onclick='copyTheDamnThing(this.id)'>" + Math.round(actualTroops.heavy.qty * hugeScavengingRatio) + "</button></td>" : '')
		       	+ "</tr></tbody></table>";
			let scavengingTable = $('#scanvengingTable');
			if (scavengingTable.length > 0) {
				scavengingTable.replaceWith(htmlToInject);
			} else {
				$('#scavenge_screen').append(htmlToInject);
			}
			$('#spearSmallButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#spearSmallButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#swordSmallButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#swordSmallButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#axeSmallButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#axeSmallButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#archerSmallButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#archerSmallButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#lightSmallButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#lightSmallButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#mArcherSmallButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#mArcherSmallButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#heavySmallButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#heavySmallButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			
			$('#spearMediumButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#spearMediumButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#swordMediumButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#swordMediumButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#axeMediumButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#axeMediumButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#archerMediumButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#archerMediumButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#lightMediumButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#lightMediumButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#mArcherMediumButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#mArcherMediumButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#heavyMediumButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#heavyMediumButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			
			$('#spearBigButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#spearBigButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#swordBigButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#swordBigButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#axeBigButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#axeBigButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#archerBigButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#archerBigButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#lightBigButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#lightBigButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#mArcherBigButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#mArcherBigButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#heavyBigButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#heavyBigButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			
			$('#spearHugeButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#spearHugeButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#swordHugeButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#swordHugeButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#axeHugeButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#axeHugeButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#archerHugeButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#archerHugeButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#lightHugeButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#lightHugeButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#mArcherHugeButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#mArcherHugeButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			$('#heavyHugeButton').click(function () {
				var $temp = $("<input>");
				$("body").append($temp);
				$temp.val($("#heavyHugeButton").text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
	      	// sendResponse(actualTroops);
	      	return true;
	    }
	  });

	function getNbTroops(troops) {
		return troops.replace(/\(|\)/g, '');
	}
